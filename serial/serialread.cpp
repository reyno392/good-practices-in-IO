#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <vector>
#include <time.h>

using namespace std;

int main(int argc, char *argv[], char* env[]) {
    // expecting arguments: 1. absolute path to file, 2. chunk size of each read
    if (argc != 3) {
        printf("Please provide file name and chunk size.\n");
        return 1;
    }

    // check if the second argument can be casted to an integer
    int chunksize = atoi(argv[2]); // overflow can occur
    if (chunksize <= 0) {
        printf("Please provide a chunk size bigger than 0.\n");
        return 1;
    }
    
    clock_t t1, t2;
    vector<char> buffer (chunksize, 0);

    // open file and check if exists
    ifstream file(argv[1], ifstream::binary); 
    if (!file) {
        printf("Please provide a file that can be opened.\n");
        return 1;
    }


    t1=clock();

    // read file by chunks
    while (file.read(buffer.data(), buffer.size())) {
        // uncomment to print out ifstream buffer
        // for (int i=0; i<buffer.size(); i++) {
        //     printf("%c", buffer[i]);
        // }
    }

    t2=clock();

    float time = ((float)t2 - (float)t1) / CLOCKS_PER_SEC;
    printf("Total time took %f\n", time);
}
