Purpose: To illustrate the difference between using I/O with different file systems (NFS vs Lustre), chunk size, thread count, I/O apis (HDF5, ParallelNetCDF, MPI_IO), and nodes.
.
.
To compile serial version: 'g++ --std=c++11 serialread.cpp -o out' with GNU/4.7.1+
To compile parallel version: 'mpic++ parallelread.cpp -o out' with GNU/4.4.5 and OpenMPI/1.4.3
