/**************************************************************************************
 *   Lucas Reynolds
 *   allproctoallproc.cpp
 *   Objective: All processors will read their section and then they will all send
 *   their data to every processor besides themself.
**************************************************************************************/

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <math.h>

using namespace std;

int main(int argc, char *argv[], char* env[]) {
    int rank, commsize;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);
    
    MPI_File infile;
    MPI_Status status;

    // expecting arguments: 1. absolute path to file, 2. chunk size of each read
    if (argc != 3) {
        if (rank==0) {printf("Please provide file name and chunk size.\n");}
        MPI_Finalize();
        exit(1);
    }

    // check if the second argument can be casted to an integer
    MPI_Offset chunksize = atoi(argv[2]); // overflow can occur
    if (chunksize <= 0) {
        if(rank==0) {printf("Please provide a chunk size bigger than 0.\n");}
        MPI_Finalize();
        exit(1);
    }

    char *buffer = (char*)malloc((chunksize+1)*sizeof(char));
    char *allbuffer = (char*)malloc((chunksize*commsize+1)*sizeof(char));

    double t1, t2;
    t1 = MPI_Wtime();

    // Try to open the file
    if (MPI_File_open(MPI_COMM_WORLD, argv[1], MPI_MODE_RDONLY, MPI_INFO_NULL, &infile)) {
        if (rank==0) {printf("Please provide a valid file name.\n");}
        MPI_Finalize();
        exit(1);
    }

    MPI_Offset filesize;
    MPI_File_get_size(infile, &filesize);
    filesize--; // get rid of eof
    int commchunksize = (int)ceil((double)filesize / commsize); // rounds too much
    MPI_Offset commstartread = rank * commchunksize;
    MPI_Offset commendread = commstartread + commchunksize;

    // printf("rank: %d, offset: %d, fs: %d, ccs: %d\n", rank, offset, filesize, commchunksize);
    // each node reads file and then sends their data to all other processors
    while (commstartread < commendread) {
        MPI_File_read_at_all(infile, commstartread, buffer, chunksize, MPI_CHAR, &status);
       
        MPI_Allgather(buffer, chunksize, MPI_CHAR, allbuffer, chunksize, MPI_CHAR, MPI_COMM_WORLD);

        commstartread += chunksize;

        // optional: print buffer on root
        // if (rank == 0) {
        //     for (int i=0; i<chunksize; i++) {
        //         printf("%c", buffer[i]);
        //     }
        // }
        
        // optional: print allbuffer on root
        // if (rank == 0) {
        //     printf("from rank %d: ", rank);
        //     for (int i=0; i<(commsize*chunksize/sizeof(char)); i++) {
        //         printf("%c", allbuffer[i]);  
        //     }
        // }

    }

    t2 = MPI_Wtime();
    MPI_Barrier(MPI_COMM_WORLD);

    if (rank == 0) {
        printf("Total time took %f\n", t2-t1);
    }

    free(buffer);
    free(allbuffer);
    MPI_File_close(&infile);
    MPI_Finalize();
}
