/**************************************************************************************
 *   Lucas Reynolds
 *   multiproctomultiproc.cpp
 *   Objective: Some processors will handle reading and some processors will handle 
 *   sending and recieving data so that all nodes will get data.
**************************************************************************************/

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <vector>
#include <time.h>

using namespace std;

int main(int argc, char *argv[], char* env[]) {
    int rank, commsize;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);
    
    MPI_File infile;
    MPI_Status status;

    // expecting arguments: 1. absolute path to file, 2. chunk size of each read
    if (argc != 3) {
        if (rank==0) {printf("Please provide file name and chunk size.\n");}
        MPI_Finalize();
        exit(1);
    }

    // check if the second argument can be casted to an integer
    MPI_Offset chunksize = atoi(argv[2]); // overflow can occur
    if (chunksize <= 0) {
        if(rank==0) {printf("Please provide a chunk size bigger than 0.\n");}
        MPI_Finalize();
        exit(1);
    }

    // vector<char>buffer (chunksize, 0);
  
    char buffer[chunksize];
    double t1, t2;
    t1 = MPI_Wtime();

    // Try to open the file
    if (MPI_File_open(MPI_COMM_WORLD, argv[1], MPI_MODE_RDONLY, MPI_INFO_NULL, &infile)) {
        if (rank==0) {printf("Please provide a valid file name.\n");}
        MPI_Finalize();
        exit(1);
    }

    MPI_Offset filesize;
    MPI_File_get_size(infile, &filesize);
    // int buffsize = filesize / commsize; // rounding error?

    // root node reads file and then sends data to all other processors
    if (rank == 0) {
        MPI_Offset offset = 0;
        while (offset < filesize) {
            MPI_File_seek(infile, offset, MPI_SEEK_SET);
            MPI_File_read(infile, buffer, chunksize/sizeof(char), MPI_CHAR, &status);
            offset += chunksize;
            MPI_Bcast(buffer, chunksize/sizeof(char), MPI_CHAR, 0, MPI_COMM_WORLD);
            printf("from rank %d: ", rank);
            for (int i=0; i<(chunksize/sizeof(char)); i++) {
                printf("%c", buffer[i]);  
            }
        }
    }

    t2 = MPI_Wtime();
    MPI_Barrier(MPI_COMM_WORLD);

    if (rank == 0) {
        printf("Total time took %f\n", t2-t1);
    }

    MPI_File_close(&infile);
    MPI_Finalize();
}
