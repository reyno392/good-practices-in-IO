To compile:

module load GNU/4.4.5
module load OpenMPI/1.7.3

mpic++ FILENAME -o out
mpirun -np THREADCOUNT out INPUTFILENAME CHUNKSIZE
