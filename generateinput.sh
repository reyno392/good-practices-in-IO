#!/bin/sh

base64 /dev/urandom | head -c 100         > file-1.txt &   # ≈ 100 b
base64 /dev/urandom | head -c 1000000     > file-2.txt &   # ≈   1 mb 
base64 /dev/urandom | head -c 10000000    > file-3.txt &   # ≈  10 mb
base64 /dev/urandom | head -c 2000000000  > file-4.txt &   # ≈   2 gb
base64 /dev/urandom | head -c 5000000000  > file-5.txt &   # ≈   5 gb
base64 /dev/urandom | head -c 15000000000 > file-6.txt &   # ≈  15 gb
